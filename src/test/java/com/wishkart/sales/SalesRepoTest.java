package com.wishkart.sales;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wishkart.sales.exceptionHandling.OrderUnsuccessfullException;
import com.wishkart.sales.models.Kart;
import com.wishkart.sales.models.KartItem;
import com.wishkart.sales.repositories.KartRepository;
import com.wishkart.sales.services.SalesService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SalesRepoTest {

	@Autowired
	private KartRepository kartRepository;
	
	@Autowired
	private SalesService salesService;

	@Test
	public void testSaveKart() {
		Kart kart = new Kart();

		KartItem kartItem = new KartItem();
		kartItem.setProductId("5b7e2e8d5c42451f06b024ec");
		kartItem.setNoOfProduct(2);
		kartItem.setRate(new BigDecimal(10000));
		kartItem.setDiscountPrcntg(new BigDecimal(0));
		kartItem.setLineTotal(kartItem.getRate().multiply(new BigDecimal(kartItem.getNoOfProduct())));

		kart.addKartItem(kartItem);
		kart.setUserId("titas.9i@gmail.com");
		kart.setNoOfItems(kart.getKartItems().size());

		Kart savedKart = kartRepository.save(kart);

		assertNotNull(savedKart.getId());
	}

	@Test
	public void testPlaceOrder() throws OrderUnsuccessfullException {

		Kart kart = new Kart();
		KartItem kartItem;
		//Add first item
		kartItem = new KartItem();
		kartItem.setProductId("5b818c2c5c424535dde2f27c");
		kartItem.setNoOfProduct(1);
		kartItem.setRate(new BigDecimal(10000));
		kartItem.setDiscountPrcntg(new BigDecimal(0));
		kartItem.setLineTotal(kartItem.getRate().multiply(new BigDecimal(kartItem.getNoOfProduct())));
		kart.addKartItem(kartItem);
		
		//add second item
		kartItem = new KartItem();
		kartItem.setProductId("5b7e2e8d5c42451f06b024ec");
		kartItem.setNoOfProduct(1);
		kartItem.setRate(new BigDecimal(225.00));
		kartItem.setDiscountPrcntg(new BigDecimal(0));
		kartItem.setLineTotal(kartItem.getRate().multiply(new BigDecimal(kartItem.getNoOfProduct())));
		kart.addKartItem(kartItem);
		
		
		kart.setUserId("munmundas10@gmail.com");
		kart.setNoOfItems(kart.getKartItems().size());
		kart.setBillAddrId(1L);
		kart.setShipAddrId(2L);

		Kart savedKart = kartRepository.save(kart);
		
		salesService.checkOut(savedKart);

	}

}
