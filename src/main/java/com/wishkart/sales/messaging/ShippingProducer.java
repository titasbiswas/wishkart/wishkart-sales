package com.wishkart.sales.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;

import com.wishkart.sales.models.external.OutboundShipping;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ShippingProducer {
	
	@Autowired
	private ShippingStreams shippingStreams;
	
	public void sendOutBoundShipping(final OutboundShipping outboundShipping) {
		log.info("sending out bound shipping ", outboundShipping);
		MessageChannel messageChannel = shippingStreams.outboundShipping();
		messageChannel.send(MessageBuilder
                .withPayload(outboundShipping)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
	}

}
