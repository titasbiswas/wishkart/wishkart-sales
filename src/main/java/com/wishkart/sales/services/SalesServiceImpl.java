package com.wishkart.sales.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.wishkart.sales.exceptionHandling.OrderUnsuccessfullException;
import com.wishkart.sales.messaging.ShippingProducer;
import com.wishkart.sales.models.Kart;
import com.wishkart.sales.models.KartItem;
import com.wishkart.sales.models.OrderDetail;
import com.wishkart.sales.models.OrderDetail.OrderDetailBuilder;
import com.wishkart.sales.models.OrderItem;
import com.wishkart.sales.models.external.OutboundShipping;
import com.wishkart.sales.repositories.KartRepository;
import com.wishkart.sales.repositories.OrderDetailRepository;

@Service
public class SalesServiceImpl implements SalesService {

	@Autowired
	private KartRepository kartRepository;
	
	@Autowired
	private OrderDetailRepository orderRepo;
	
	@Autowired
	private ShippingProducer shippingProducer;

	@Transactional
	public Kart addToKart(KartItem kartItem, String userId) {
		Kart kart;
		Optional<Kart> optKart = kartRepository.findByUserId(userId);
		if (optKart.isPresent()) {
			kart = optKart.get();
			kart.addKartItem(kartItem);
			kart.setNoOfItems(kart.getKartItems().size());
		} else {
			kart = new Kart();
			kart.setUserId(userId);
			kart.addKartItem(kartItem);
			kart.setNoOfItems(kart.getKartItems().size());
		}

		return kartRepository.save(kart);

	}

	@Override
	@Transactional
	public Kart addAddress(Kart kart) {
		return kartRepository.save(kart);
	}

	@Override
	public Kart addPayment(Kart kart) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Integer placeOrder(Kart kart) {
		// TODO Check payment service with userid and payment id
		return null;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public String checkOut(Kart kart) throws OrderUnsuccessfullException {
		String orderId = null;
		try {
			validateKart(kart);
			validatePayment(kart);
			creteateOrder(kart);
		} catch (OrderUnsuccessfullException oe) {
			initiateRevertPayment(kart);
			throw oe;
		}

		return orderId;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private void initiateRevertPayment(Kart kart) {
		// TODO call the payment service and revert the payment

	}

	@Transactional(propagation = Propagation.MANDATORY)
	private void creteateOrder(Kart kart) throws OrderUnsuccessfullException {
		OrderDetail orderDetail;
		try {
			List<OrderItem> orderItems = kart.getKartItems().stream().map(k -> {
				OrderItem item = new OrderItem();
				item.setAmount(k.getRate());
				item.setDiscountPrcntg(k.getDiscountPrcntg());
				item.setProductId(k.getProductId());
				item.setQty(k.getNoOfProduct());
				item.setRate(k.getRate());
				return item;
			}).collect(Collectors.toList());

			orderDetail = OrderDetail.builder()
					.billAddrId(kart.getBillAddrId())
					.orderDate(new Timestamp(System.currentTimeMillis()))
					.shipAddrId(kart.getShipAddrId())
					.totalAmount(calculateOrderTotal(kart.getKartItems()))
					.orderItems(orderItems).build();
			
			orderDetail = orderRepo.save(orderDetail);			
		} catch (Exception e) {
			throw new OrderUnsuccessfullException("Errror occurrd while creating order", e);
		}
		
		try {
			placeShippingOrder(orderDetail);
		} catch (Exception e2) {
			throw new OrderUnsuccessfullException("Errror occurrd while placinng shipping order", e2);
		}

	}

	private void placeShippingOrder(OrderDetail orderDetail) {
		OutboundShipping outboundShipping;
		for(OrderItem orderItem : orderDetail.getOrderItems()) {
			outboundShipping = OutboundShipping.builder()
					.billAddrId(orderDetail.getBillAddrId())
					.itemId(orderItem.getId())
					.orderId(orderDetail.getId())
					.productId(orderItem.getProductId())
					.qty(orderItem.getQty())
					.shipAddrId(orderDetail.getShipAddrId())
					.build();
			shippingProducer.sendOutBoundShipping(outboundShipping);
		}
		
	}

	private BigDecimal calculateOrderTotal(List<KartItem> kartItems) {
		BigDecimal sum;
		sum = kartItems.stream().map(k -> k.getLineTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);
		return sum;
	}

	private void validatePayment(Kart kart) throws OrderUnsuccessfullException {
		// TODO validate payment from the payment service

	}

	private void validateKart(Kart kart) throws OrderUnsuccessfullException {
		// TODO add validating cart busineess logic

	}

}
