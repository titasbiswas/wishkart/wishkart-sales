package com.wishkart.sales.models;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the kart database table.
 * 
 */
@Entity
@Data
@NamedQuery(name="Kart.findAll", query="SELECT k FROM Kart k")
public class Kart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="no_of_items")
	private Integer noOfItems;

	@Column(name="user_id")
	private String userId;
	
	@Column(name="ship_addr_id")
	private Long shipAddrId;

	@Column(name="bill_addr_id")
	private Long billAddrId;
	
	@Column(name="payment_id")
	private String paymentId;

	//bi-directional many-to-one association to KartItem
	@OneToMany(mappedBy="kart", cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
	private List<KartItem> kartItems = new ArrayList<>();

	public Kart() {
	}


	public KartItem addKartItem(KartItem kartItem) {
		getKartItems().add(kartItem);
		kartItem.setKart(this);

		return kartItem;
	}

	public KartItem removeKartItem(KartItem kartItem) {
		getKartItems().remove(kartItem);
		kartItem.setKart(null);

		return kartItem;
	}

}