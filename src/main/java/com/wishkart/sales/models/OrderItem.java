package com.wishkart.sales.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the order_items database table.
 * 
 */
@Entity
@Table(name="order_items")
@NamedQuery(name="OrderItem.findAll", query="SELECT o FROM OrderItem o")
@Data
public class OrderItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private BigDecimal amount;

	@Column(name="delivered_on")
	private Timestamp deliveredOn;

	@Column(name="delivery_comment")
	private String deliveryComment;

	@Column(name="discount_prcntg")
	private BigDecimal discountPrcntg;

	@Column(name="product_id")
	private String productId;

	private Integer qty;

	private BigDecimal rate;

	//bi-directional many-to-one association to OrderDetail
	@ManyToOne
	@JoinColumn(name="order_id")
	@JsonIgnore
	private OrderDetail orderDetail;

	public OrderItem() {
	}

	

}